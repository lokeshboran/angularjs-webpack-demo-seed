var webpack = require('webpack'),
path = require('path');

var _path = {
    app : path.resolve(__dirname,'app')
};

module.exports = {
    context : _path.app,
    entry : {
        app : ['webpack/hot/dev-server','./core/bootstrap.js']
    },
    plugins : [new webpack.HotModuleReplacementPlugin()],
    output : {
        path : _path.app,
        filename : 'bundle.js'
    },
    module : {
        loaders : [
            {
                test: /\.css$/,
                loader: 'css'
            },
            {
                test: /\.js$/,
                loader: 'ng-annotate',
                exclude: /node_modules/
            }
        ]
    }

};
