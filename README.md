# angularjs-webpack-demo-seed

This project is an application skeleton for [AngularJS](http://angularjs.org/) 1.5 web app.

It uses bunling systems webpack.

## Getting Started

To get you started you can simply clone the angularjs-webpack-demo-seed repository and install the dependencies:

### Prerequisites

You need git to clone the angularjs-webpack-demo-seed repository. You can get git from
[https://lokeshboran@gitlab.com/lokeshboran/angularjs-webpack-demo-seed.git](https://lokeshboran@gitlab.com/lokeshboran/angularjs-webpack-demo-seed.git).

We also use a number of node.js tools to initialize and test angular-seed. You must have node.js and
its package manager (npm) installed.  You can get them from [http://nodejs.org/](http://nodejs.org/).

Install these globally:

```
npm install -g webpack webpack-dev-server
```

### Clone angularjs-webpack-demo-seed

Clone the angularjs-webpack-demo-seed repository using git:

### Install Dependencies

We have two kinds of dependencies in this project: development tools and application specific packages. They are both managed with npm in package.json as devDependencies and dependencies respectively.

```
npm install
```

## Directory Layout

```
app/                    --> all of the source files for the application
  core/                --> app bootstrapping files 
  index.js                --> main application module
  index.css                --> main application css file
  index.html            --> app layout file (the main html template file of the app)
```

### Running the App

The angularjs-webpack-demo-seed project comes preconfigured with a local development webserver. It is a webpack-dev-server, that supports hot reload.  You can start this webserver with `npm start`.

Now browse to the app at `http://localhost:8045/`.
